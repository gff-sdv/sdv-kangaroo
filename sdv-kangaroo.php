<?php

/*
Plugin Name: SDV Kangaroo
Description: Provides the GFF SDV Kangaroo.
Version: 1.1
Author: GFF Integrative Kommunikation GmbH
Author URI: https://www.gff.ch/
License: Proprietary
*/

declare( strict_types=1 );

const SDV_KANGAROO_JS_VERSION = '1.1';

const SDV_KANGAROO_CSS_VERSION = '1.1';

add_action( 'init', function () {
	wp_register_script( 'sdv-kangaroo', plugins_url( 'scripts/kangaroo.js', __FILE__ ), [], SDV_KANGAROO_JS_VERSION );
	wp_register_style( 'sdv-kangaroo', plugins_url( 'styles/kangaroo.css', __FILE__ ), [], SDV_KANGAROO_CSS_VERSION );
	wp_localize_script( 'sdv-kangaroo', 'sdv_kangaroo', [
		'image_url' => plugins_url( 'assets/kangaroo.svg', __FILE__ ),
	] );
} );

add_action( 'wp_enqueue_scripts', function () {
	wp_enqueue_script( 'sdv-kangaroo' );
	wp_enqueue_style( 'sdv-kangaroo' );
} );

////////////////////// Update Checker //////////////////////

add_action( 'plugins_loaded', function () {
	if ( ! class_exists( Puc_v4_Factory::class ) ) {
		return;
	}

	$group = 'gff-sdv';
	$slug  = 'sdv-kangaroo';

	$updateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/' . $group . '/' . $slug . '/',
		__FILE__,
		basename( __FILE__, '.php' )
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcsApi */
	$vcsApi = $updateChecker->getVcsApi();
	$vcsApi->enableReleasePackages();
} );
