=== SDV Kangaroo ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.0
Tested up to: 6.0
Requires PHP: 7.4
Stable tag: 1.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides the SDV Kangaroo.

== Description ==

This plugin provides the GFF SDV Kangaroo.

== Frequently Asked Questions ==

None.

== Upgrade Notice ==

Please upgrade as soon as possible.

== Changelog ==

= 1.1 =
* Bigger kangaroo.

= 1.0 =
* First official version.
