(function (sdv_kangaroo) {
  function domReady (fn) {
    document.addEventListener('DOMContentLoaded', fn);
    if (document.readyState === 'interactive' || document.readyState === 'complete') {
      fn();
    }
  }

  domReady(() => {
    const image_path = sdv_kangaroo['image_url'];

    const get_offset = (el) => {
      const rect = el.getBoundingClientRect();
      return {
        left: rect.left + window.scrollX,
        top: rect.top + window.scrollY
      };
    };

    const get_target_x = () => {
      const el = document.querySelector('#cb-row--header-bottom .customify-container');
      return get_offset(el).left;
    };

    const get_target_y = () => {
      const g = document.getElementById('cb-row--header-bottom');
      return get_offset(g).top;
    };

    const body = document.querySelector('body');

    const element = document.createElement('div');
    element.classList.add('kangaroo-container');
    element.classList.add('hide-on-mobile');
    element.classList.add('hide-on-tablet');

    const image = document.createElement('img');
    image.src = image_path;
    image.alt = 'Kangaroo';

    element.appendChild(image);

    body.appendChild(element);

    let x = -100;
    let y = get_target_y();

    let interval_duration = 1 / 25; // seconds
    let gravity = 400; // pixels per second

    let floor = get_target_y();
    let target_x = get_target_x();
    let target_y = get_target_y();

    let requested_distance_x = (target_x - x) / 2;

    let theta = 60 * Math.PI / 180;
    let speed = Math.sqrt(requested_distance_x * gravity / Math.sin(2 * theta));

    let speed_x = speed * Math.cos(theta); // pixels per second
    let speed_y = -speed * Math.sin(theta); // pixels per second

    element.style.transition = 'transform ' + interval_duration + 's linear';
    element.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
    setInterval(() => {
      floor = get_target_y();

      let target_changed = false;
      if (get_target_x() !== target_x) {
        target_x = get_target_x();
        target_changed = true;
      }
      if (get_target_y() !== target_y) {
        target_y = get_target_y();
        target_changed = true;
      }

      if (!isNaN(speed_x)) {
        x = x + speed_x * interval_duration;
      }

      if (!isNaN(speed_y)) {
        y = y + speed_y * interval_duration + 0.5 * gravity * Math.pow(interval_duration, 2);
        speed_y = speed_y + gravity * interval_duration;
      }

      // let jump_speed = 10;
      if (y >= floor || target_changed) {
        requested_distance_x = (target_x - x) / 2;

        if (requested_distance_x >= 0) {
          theta = 60 * Math.PI / 180;
        } else {
          theta = 120 * Math.PI / 180;
        }

        speed = Math.sqrt(requested_distance_x * gravity / Math.sin(2 * theta));
        if (speed < 10) {
          speed = 0;
        }

        speed_x = speed * Math.cos(theta); // pixels per second
        speed_y = -speed * Math.sin(theta); // pixels per second
      }

      if (y >= floor) {
        y = floor;
      }

      element.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
    }, interval_duration * 1000);

    let h = 220;

    image.style.height = h + 'px';
    image.style.left = '0px';
    image.style.top = '-' + (0.8 * h) + 'px';
  });
})(sdv_kangaroo);
