module.exports = function (grunt) {
  // noinspection JSUnresolvedFunction
  grunt.initConfig({
    compress: {
      main: {
        options: {
          archive: function () {
            return '../sdv-kangaroo.zip';
          },
          mode: 'zip'
        },
        files: [
          {
            expand: true,
            cwd: '../',
            src: [
              '**',
              '!_dev/**',
              '!assets/*.xcf',
              '!.gitignore',
              '!.gitlab-ci.yml',
              '!README.md',
            ],
            dest: 'sdv-kangaroo/'
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-compress');

  // Register the tasks.
  grunt.registerTask('archive', ['compress']);
};
